add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
add_cus_dep('acn', 'acr', 0, 'makeglo2acn');
add_cus_dep('fmo', 'fmi', 0, 'makeglo2fmo');
sub makeglo2gls {
    system("makeglossaries $_[0].glo");
}
sub makeglo2fmo {
    system("makeglossaries $_[0].fmo");
}
sub makeglo2acn {
    system("makeglossaries $_[0].acn");
}

$cleanup_includes_cusdep_generated=1;
$bibtex_use = 2;
$clean_ext = 'synctex.gz synctex.gz(busy) run.xml tex.bak bbl bcf fdb_latexmk run tdo %R-blx.bib glo gls glg acn acr fmo fmi ist lol alg fml'
