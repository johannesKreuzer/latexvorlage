DOCUMENT_NAME = dokumentation
BACKEND = xelatex

# Build the LaTeX document.
all: report cleanup

# Remove output directory.
clean:
	latexmk -C 

# cleanup tempfiles
cleanup:
	latexmk -c

# Generate PDF output from LaTeX input files.
report:
	latexmk -e "$pdflatex=q/$(BACKEND) -synctex=1 -interaction=nonstopmode/" -bibtex -pdf $(DOCUMENT_NAME).tex
