## Anforderungen:
TexLive Version 2014 oder neuer

Zusätzlich zu den unten angegebenen Latex Paketen werden noch folgende Programme benötigt (alle Teil der TexLive Distribution):

- xelatex 

- biber

- latexmk

- makeglossaries


## Vorlage verwenden
Zur Kompilierung folgenden Befehl ausführen:

    $ latexmk -e "$pdflatex=q/xelatex -synctex=1 -interaction=nonstopmode/" -bibtex -pdf dokumentation.tex
	
	oder unter Linux einfach make aufrufen.
	unter Windows ist es auch möglich, dass man einfach die make.bat startet.

## Benötigte Dateien
	scrreprt.cls    		2013/12/19 v3.12 KOMA-Script document class (report)
	scrkbase.sty    		2013/12/19 v3.12 KOMA-Script package (KOMA-Script-dependent basics and keyval usage)
	 scrbase.sty    		2013/12/19 v3.12 KOMA-Script package (KOMA-Script-independent basics and keyval usage)
	  keyval.sty    		2014/05/08 v1.15 key=value parser (DPC)
	scrlfile.sty    		2013/12/19 v3.12 KOMA-Script package (loading files)
	tocbasic.sty    		2013/12/19 v3.12 KOMA-Script package (handling toc-files)
	scrsize12pt.clo    		2013/12/19 v3.12 KOMA-Script font size class option (12pt)
	typearea.sty    		2013/12/19 v3.12 KOMA-Script package (type area)
	geometry.sty    		2010/09/12 v5.6 Page Geometry
	   ifpdf.sty    		2011/01/30 v2.3 Provides the ifpdf switch (HO)
	  ifvtex.sty    		2010/03/01 v1.5 Detect VTeX and its facilities (HO)
	 ifxetex.sty    		2010/09/12 v0.6 Provides ifxetex conditional
	fullpage.sty    		1999/02/23 1.1 (PWD)
	 fontenc.sty
	   t1enc.def    		2005/09/27 v1.99g Standard LaTeX file
	fontspec.sty    		2013/05/20 v2.3c Font selection for XeLaTeX and LuaLaTeX
	   expl3.sty    		2014/05/20 v4814 L3 programming layer (loader) 
	expl3-code.tex    		2014/05/20 v4814 L3 programming layer 
		etex.sty    		1998/03/26 v2.0 eTeX basic definition package (PEB)
	  xparse.sty    		2014/05/05 v4740 L3 Experimental document command parser
	fontspec-patches.sty    2013/05/20 v2.3c Font selection for XeLaTeX and LuaLaTeX
	fixltx2e.sty    		2014/05/13 v1.1q fixes to LaTeX
	fontspec-xetex.sty    	2013/05/20 v2.3c Font selection for XeLaTeX and LuaLaTeX
	 fontenc.sty
	  eu1enc.def    		2010/05/27 v0.1h Experimental Unicode font encodings
	  eu1lmr.fd    			2009/10/30 v1.6 Font defs for Latin Modern
	xunicode.sty    		2011/09/09 v0.981 provides access to latin accents and many other characters in Unicode lower plane
	 eu1lmss.fd    			2009/10/30 v1.6 Font defs for Latin Modern
	graphicx.sty    		2014/04/25 v1.0g Enhanced LaTeX Graphics (DPC,SPQR)
	graphics.sty    		2009/02/05 v1.0o Standard LaTeX Graphics (DPC,SPQR)
		trig.sty    		1999/03/16 v1.09 sin cos tan (DPC)
	graphics.cfg    		2010/04/23 v1.9 graphics configuration of TeX Live
	   xetex.def    		2014/04/28 v4.01 LaTeX color/graphics driver for XeTeX (RRM/JK)
	fontspec.cfg
	 xltxtra.sty    		2010/09/20 v0.5e Improvements for the "XeLaTeX" format
	ifluatex.sty    		2010/03/01 v1.3 Provides the ifluatex switch (HO)
	realscripts.sty    		2013/03/18 v0.3c Access OpenType subscripts and superscripts
	metalogo.sty    		2010/05/29 v0.12 Extended TeX logo macros
	setspace.sty    		2011/12/19 v6.7a set line spacing
	 makeidx.sty    		2000/03/29 v1.0m Standard LaTeX package
	   babel.sty    		2014/03/24 3.9k The Babel package
	 ngerman.ldf    		2013/12/13 v2.7 German support for babel (new orthography)
	csquotes.sty    		2011/10/22 v5.1d context-sensitive quotations
	etoolbox.sty    		2011/01/03 v2.1 e-TeX tools for LaTeX
	csquotes.def    		2011/10/22 v5.1d csquotes generic definitions
	csquotes.cfg    
	   float.sty    		2001/11/08 v1.3d Float enhancements (AL)
	longtable.sty    		2004/02/01 v4.11 Multi-page Table package (DPC)
	floatflt.sty    		1997/07/16 v. 1.31
	 amsmath.sty    		2013/01/14 v2.14 AMS math features
	 amstext.sty    		2000/06/29 v2.01
	  amsgen.sty    		1999/11/30 v2.0
	  amsbsy.sty    		1999/11/29 v1.2d
	  amsopn.sty    		1999/12/14 v2.01 operator names
	 amssymb.sty    		2013/01/14 v3.01 AMS font symbols
	amsfonts.sty    		2013/01/14 v3.01 Basic AMSFonts support
	 SIunits.sty    		2007/12/02 v1.36 Support for the International System of units (MH)
	 SIunits.cfg
	rotating.sty    		2009/03/28 v2.16a rotated objects in LaTeX
	  ifthen.sty    		2001/05/26 v1.1c Standard LaTeX ifthen package (DPC)
	pdflscape.sty    		2008/08/11 v0.10 Display of landscape pages in PDF (HO)
	  lscape.sty    		2000/10/22 v3.01 Landscape Pages (DPC)
	atbegshi.sty    		2011/10/05 v1.16 At begin shipout hook (HO)
	infwarerr.sty    		2010/04/08 v1.3 Providing info/warning/error messages (HO)
	 ltxcmds.sty    		2011/11/09 v1.22 LaTeX kernel commands for general use (HO)
	  xcolor.sty    		2007/01/21 v2.11 LaTeX color extensions (UK)
	   color.cfg    		2007/01/18 v1.5 color configuration of teTeX/TeXLive
	dvipsnam.def    		2014/04/23 v3.0j Driver-dependant file (DPC,SPQR)
	colortbl.sty    		2012/02/13 v1.0a Color table columns (DPC)
	   array.sty    		2008/09/09 v2.4c Tabular extension package (FMi)
	listings.sty    		2014/03/04 1.5c (Carsten Heinz)
	 lstmisc.sty    		2014/03/04 1.5c (Carsten Heinz)
	listings.cfg    		2014/03/04 1.5c listings configuration
	lstlang1.sty    		2014/03/04 1.5c listings language file
	lstlang2.sty    		2014/03/04 1.5c listings language file
	 caption.sty    		2013/05/02 v3.3-89 Customizing captions (AR)
	caption3.sty    		2013/05/02 v1.6-88 caption3 kernel (AR)
	ltcaption.sty    		2013/02/03 v1.3-62 longtable captions (AR)
	fancyvrb.sty    		2008/02/07
	scrpage2.sty    		2013/12/19 v3.12 KOMA-Script package (page head and foot)
	todonotes.sty    		2012/07/25
	 xkeyval.sty    		2014/05/09 v2.6d package option processing (HA)
	 xkeyval.tex    		2014/05/09 v2.6d key=value parser (HA)
		tikz.sty    		2013/12/13 v3.0.0 (rcs-revision 1.142)
		 pgf.sty    		2013/12/18 v3.0.0 (rcs-revision 1.14)
	  pgfrcs.sty    		2013/12/20 v3.0.0 (rcs-revision 1.28)
	everyshi.sty    		2001/05/15 v3.00 EveryShipout Package (MS)
	  pgfrcs.code.tex
	 pgfcore.sty    		2010/04/11 v3.0.0 (rcs-revision 1.7)
	  pgfsys.sty    		2013/11/30 v3.0.0 (rcs-revision 1.47)
	  pgfsys.code.tex
	pgfsyssoftpath.code.tex    2013/09/09  (rcs-revision 1.9)
	pgfsysprotocol.code.tex    2006/10/16  (rcs-revision 1.4)
	 pgfcore.code.tex
	pgfcomp-version-0-65.sty    2007/07/03 v3.0.0 (rcs-revision 1.7)
	pgfcomp-version-1-18.sty    2007/07/23 v3.0.0 (rcs-revision 1.1)
	  pgffor.sty    		2013/12/13 v3.0.0 (rcs-revision 1.25)
	 pgfkeys.sty    
	 pgfkeys.code.tex
	 pgfmath.sty    
	 pgfmath.code.tex
	  pgffor.code.tex
		tikz.code.tex
		calc.sty    		2007/08/22 v4.3 Infix arithmetic (KKT,FJ)
	glossaries.sty    		2014/04/04 v4.07 (NLCT)
	mfirstuc.sty    		2013/11/04 v1.08 (NLCT)
	textcase.sty    		2004/10/07 v0.07 Text only upper/lower case changing (DPC)
		xfor.sty    		2009/02/05 v1.05 (NLCT)
	datatool-base.sty    	2013/09/06 v2.18 (NLCT)
	  substr.sty    		2009/10/20 v1.2 Handle substrings
	datatool-fp.sty    		2013/08/29 v2.17 (NLCT)
		  fp.sty    		1995/04/02
	defpattern.sty    		1994/10/12
	fp-basic.sty    		1996/05/13
	fp-addons.sty    		1995/03/15
	 fp-snap.sty    		1995/04/05
	  fp-exp.sty    		1995/04/03
	fp-trigo.sty    		1995/04/14
	  fp-pas.sty    		1994/08/29
	fp-random.sty    		1995/02/23
	  fp-eqn.sty    		1995/04/03
	  fp-upn.sty    		1996/10/21
	 fp-eval.sty    		1995/04/03
	glossaries-compatible-307.sty    2013/11/14 v4.0 (NLCT)
	translator.sty    		2010/06/12 ver 1.10
	translator-language-mappings.tex
	glossary-hypernav.sty    2013/11/14 v4.0 (NLCT)
	glossary-list.sty    	2013/11/14 v4.0 (NLCT)
	glossary-long.sty    	2013/11/14 v4.0 (NLCT)
	glossary-super.sty    	2013/11/14 v4.0 (NLCT)
	supertabular.sty    	2004/02/20 v4.1e the supertabular environment
	glossary-tree.sty    	2014/03/06 v4.04 (NLCT)
	enumitem.sty    		2011/09/28 v3.5.2 Customized lists
	footmisc.sty    		2011/06/06 v5.5b a miscellany of footnote facilities
	biblatex.sty    		2013/11/25 v2.8a programmable bibliographies (PK/JW/AB)
	biblatex2.sty    		2013/11/25 v2.8a programmable bibliographies (biber) (PK/JW/AB)
	kvoptions.sty    		2011/06/30 v3.11 Key value format for package options (HO)
	kvsetkeys.sty   		2012/04/25 v1.16 Key value parser (HO)
	etexcmds.sty    		2011/02/16 v1.5 Avoid name clashes with e-TeX commands (HO)
	  logreq.sty    		2010/08/04 v1.0 xml request logger
	  logreq.def    		2010/08/04 v1.0 logreq spec v1.0
		 url.sty    		2013/09/16  ver 3.4  Verb mode for urls, etc.
	  blx-dm.def
	alphabetic.dbx
	biblatex-dm.cfg
	blx-compat.def    		2013/11/25 v2.8a biblatex compatibility (PK/JW/AB)
	biblatex.def    
	blx-natbib.def    		2013/11/25 v2.8a biblatex compatibility (PK/JW/AB)
	standard.bbx    		2013/11/25 v2.8a biblatex bibliography style (PK/JW/AB)
	alphabetic.bbx    		2013/11/25 v2.8a biblatex bibliography style (PK/JW/AB)
	alphabetic.cbx    		2013/11/25 v2.8a biblatex citation style (PK/JW/AB)
	biblatex.cfg    
	scrextend.sty    
	remreset.sty    
	hyperref.sty    		2012/11/06 v6.83m Hypertext links for LaTeX
	hobsub-hyperref.sty    	2012/05/28 v1.13 Bundle oberdiek, subset hyperref (HO)
	hobsub-generic.sty    	2012/05/28 v1.13 Bundle oberdiek, subset generic (HO)
	  hobsub.sty    		2012/05/28 v1.13 Construct package bundles (HO)
	 intcalc.sty    		2007/09/27 v1.1 Expandable calculations with integers (HO)
	kvdefinekeys.sty    	2011/04/07 v1.3 Define keys (HO)
	pdftexcmds.sty    		2011/11/29 v0.20 Utility functions of pdfTeX for LuaTeX (HO)
	pdfescape.sty    		2011/11/25 v1.13 Implements pdfTeX's escape features (HO)
	bigintcalc.sty    		2012/04/08 v1.3 Expandable calculations on big integers (HO)
	  bitset.sty    		2011/01/30 v1.1 Handle bit-vector datatype (HO)
	uniquecounter.sty    	2011/01/30 v1.2 Provide unlimited unique counter (HO)
	letltxmacro.sty    		2010/09/02 v1.4 Let assignment for LaTeX macros (HO)
	 hopatch.sty    		2012/05/28 v1.2 Wrapper for package hooks (HO)
	xcolor-patch.sty    	2011/01/30 xcolor patch
	atveryend.sty    		2011/06/30 v1.8 Hooks at the very end of document (HO)
	refcount.sty    		2011/10/16 v3.4 Data extraction from label references (HO)
	 hycolor.sty    		2011/01/30 v1.7 Color options for hyperref/bookmark (HO)
	 auxhook.sty    		2011/03/04 v1.3 Hooks for auxiliary files (HO)
	  pd1enc.def    		2012/11/06 v6.83m Hyperref: PDFDocEncoding definition (HO)
	hyperref.cfg    		2002/06/06 v1.2 hyperref configuration of TeXLive
	   puenc.def    		2012/11/06 v6.83m Hyperref: PDF Unicode definition (HO)
	  hxetex.def    		2012/11/06 v6.83m Hyperref driver for XeTeX
	stringenc.sty    		2011/12/02 v1.10 Convert strings between diff. encodings (HO)
	rerunfilecheck.sty    	2011/04/15 v1.7 Rerun checks for auxiliary files (HO)
	  german.lbx    		2013/11/25 v2.8a biblatex localization (PK/JW/AB)
	 ngerman.lbx    		2013/11/25 v2.8a biblatex localization (PK/JW/AB)
	   t3cmr.fd    			2001/12/31 TIPA font definitions
	  ts1cmr.fd    			1999/05/25 v2.5h Standard LaTeX font definitions
	glossaries-dictionary-English.dict    
	glossaries-dictionary-German.dict    
	 nameref.sty    		2012/10/27 v2.43 Cross-referencing by name of section
	gettitlestring.sty    	2010/12/03 v1.4 Cleanup title references (HO)
		umsa.fd    			2013/01/14 v3.01 AMS symbols A
		umsb.fd    			2013/01/14 v3.01 AMS symbols B
